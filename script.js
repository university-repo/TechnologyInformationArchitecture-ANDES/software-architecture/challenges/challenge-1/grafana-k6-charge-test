import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
    vus: 20,
    duration: '120s',
}

export default function () {
    const url = 'http://localhost:9081/v1/product/transaction';

    const payload = JSON.stringify({
        'message': 'Hola me llamo patroclo'
    })

    const params = {
        headers: {
            'X-Name': 'api-test',
            'X-RqUID': '1234567890',
            'Content-Type': 'application/json' // Ensure there are no extra spaces
        }
    };

    http.post(url, payload, params);
    sleep(1);
}