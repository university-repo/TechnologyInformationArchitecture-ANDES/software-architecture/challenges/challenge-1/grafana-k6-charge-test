# Grafana K6 Charge - test

The main goal of this repository is to save documentation and installation for Grafana K6 Charge test for multiple uses.

## Installation

```bash
sudo gpg -k
sudo gpg --no-default-keyring --keyring /usr/share/keyrings/k6-archive-keyring.gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C5AD17C747E3415A3642D57D77C6C491D6AC1D69
echo "deb [signed-by=/usr/share/keyrings/k6-archive-keyring.gpg] https://dl.k6.io/deb stable main" | sudo tee /etc/apt/sources.list.d/k6.list
sudo apt-get update
sudo apt-get install k6
```

## Run

```bash
k6 run script.js
```

## Members

- Joann Castellanos
- Juan Nicolas Silva
- Andrés Guerrero
- Andrés Felipe Wilches Torres
